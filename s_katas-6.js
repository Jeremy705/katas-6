/* 
 * 1. Convert this to an arrow function
 */
function add(x, y) {
    return x + y;
}

let katas1 = (x, y) => x + y;

add(3,4)
katas1(3,4)

/*
 * 2. If a function only has one argument, you can exclude the parentheses.
 *    Convert this to an arrow function that takes advantage of that.
 */
function increment(number) {
    return number + 1;
}

let katas2 = number => number + 1;

increment(1)
katas2(1)

/* 
 * 3. If a function's body includes a single expression, you can drop the curly
 *    braces and the return statement. Update this arrow function to be a one
 *    liner which doesn't include "return".
 */
const decrement = number => {
    return number - 1;
};

const katas3 = number => number - 1;

module.exports = {
    add,
    increment,
    decrement
};
